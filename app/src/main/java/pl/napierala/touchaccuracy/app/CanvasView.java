package pl.napierala.touchaccuracy.app;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class CanvasView extends View {

    public int width;
    public int height;
    private Bitmap mBitmap;
    private Canvas mCanvas;

    private List<TouchCrosshair> touchCrosshairs = new ArrayList<>();

    Context context;

    public CanvasView(Context c, AttributeSet attrs) {
        super(c, attrs);
        context = c;
    }

    // override onSizeChanged
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // your Canvas will draw onto the defined Bitmap
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        synchronized (touchCrosshairs) { // Synchronize on this object so that the motion event and drawing work just fine
            for (TouchCrosshair touchCrosshair : touchCrosshairs) {
                touchCrosshair.draw(canvas);
            }
        }
    }

    public void clearCanvas() {

        synchronized (touchCrosshairs) {
            touchCrosshairs.clear();
        }
        invalidate();
    }


    private void getPointersInfoAndUpdateTouchCrosshairs(MotionEvent event) {

        synchronized (touchCrosshairs) { // Synchronize on this object so that the motion event and drawing work just fine

            touchCrosshairs.clear();

            // For through all of the pointers
            for (int i = 0; i < event.getPointerCount(); i++) {

                MotionEvent.PointerCoords pointerCoords = new MotionEvent.PointerCoords();
                event.getPointerCoords(i, pointerCoords);

                MotionEvent.PointerProperties pointerProperties = new MotionEvent.PointerProperties();
                event.getPointerProperties(i, pointerProperties);

                TouchCrosshair touchCrosshair = new TouchCrosshair(pointerCoords, pointerProperties);
                touchCrosshairs.add(touchCrosshair);
            }
        }
    }

    //override the onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        getPointersInfoAndUpdateTouchCrosshairs(event);

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                clearCanvas();
                break;
        }

        // Whatever happened invalidate anyway
        invalidate();

        return true;
    }
}
