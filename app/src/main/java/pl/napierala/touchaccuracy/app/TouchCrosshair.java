package pl.napierala.touchaccuracy.app;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

/**
 * Created by jedrzej on 22.01.2016.
 */
public class TouchCrosshair {

    private static final float CROSSHAIR_SIZE_IN_PIXELS = 40;

    private MotionEvent.PointerCoords pointerCoords = null;
    private MotionEvent.PointerProperties pointerProperties = null;

    public TouchCrosshair() {
    }

    public TouchCrosshair(MotionEvent.PointerCoords pointerCoords, MotionEvent.PointerProperties pointerProperties) {
        this.pointerCoords = pointerCoords;
        this.pointerProperties = pointerProperties;
    }


    /**
     * <p>
     * Draw the crosshair on the <code>canvas</code> argument.
     * The canvas that will be draw consists of a circle and two lines(one horizontal and one vertical)
     * </p>
     *
     * @param canvas The canvas to the draw the crosshair on.
     */
    public void draw(Canvas canvas) {

        if (pointerCoords == null) {
            return;
        }

        drawInnerCircle(canvas);
        drawHorizontalAndVerticalLines(canvas);
        drawTextInfoAboutTheCrosshair(canvas);

    }

    private void drawInnerCircle(Canvas canvas) {

        Paint innerCirclePaint = new Paint();
        innerCirclePaint.setStyle(Paint.Style.STROKE);
        innerCirclePaint.setColor(Color.RED);

        // Finally draw the actual circle
        canvas.drawCircle(pointerCoords.getAxisValue(MotionEvent.AXIS_X), pointerCoords.getAxisValue(MotionEvent.AXIS_Y),
                CROSSHAIR_SIZE_IN_PIXELS, innerCirclePaint);

    }

    private void drawHorizontalAndVerticalLines(Canvas canvas) {

        // Paint
        Paint linePaint = new Paint();
        linePaint.setStyle(Paint.Style.FILL);
        linePaint.setColor(Color.RED);


        /* Horizontal */

        // The line will start before the circle
        float horizLineXStart = pointerCoords.getAxisValue(MotionEvent.AXIS_X) - CROSSHAIR_SIZE_IN_PIXELS - (CROSSHAIR_SIZE_IN_PIXELS / 3);

        // The line will end after the circle
        float horizLineXEnd = pointerCoords.getAxisValue(MotionEvent.AXIS_X) + CROSSHAIR_SIZE_IN_PIXELS + (CROSSHAIR_SIZE_IN_PIXELS / 3);

        float horizLineY = pointerCoords.getAxisValue(MotionEvent.AXIS_Y);

        canvas.drawLine(horizLineXStart, horizLineY, horizLineXEnd, horizLineY, linePaint);

        /* Vertical */

        float verticalLineX = pointerCoords.getAxisValue(MotionEvent.AXIS_X);

        float verticalLineYStart = pointerCoords.getAxisValue(MotionEvent.AXIS_Y) - CROSSHAIR_SIZE_IN_PIXELS - (CROSSHAIR_SIZE_IN_PIXELS / 3);
        float verticalLineYEnd = pointerCoords.getAxisValue(MotionEvent.AXIS_Y) + CROSSHAIR_SIZE_IN_PIXELS + (CROSSHAIR_SIZE_IN_PIXELS / 3);



        canvas.drawLine(verticalLineX, verticalLineYStart, verticalLineX, verticalLineYEnd, linePaint);

    }

    private void drawTextInfoAboutTheCrosshair(Canvas canvas) {
        Paint textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextSize(20);

        //Close to the vertical line top position
        float textX = pointerCoords.getAxisValue(MotionEvent.AXIS_X);
        float textY = pointerCoords.getAxisValue(MotionEvent.AXIS_Y) + CROSSHAIR_SIZE_IN_PIXELS + (CROSSHAIR_SIZE_IN_PIXELS / 3) + (CROSSHAIR_SIZE_IN_PIXELS / 6);

        canvas.drawText("X:" + pointerCoords.getAxisValue(MotionEvent.AXIS_X) +
                " Y:" + pointerCoords.getAxisValue(MotionEvent.AXIS_Y), textX, textY, textPaint);
    }
}
